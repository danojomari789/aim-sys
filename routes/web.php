<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// <!-- 
// | =================================================================================
// |                      Login Routes
// | =================================================================================
// -->

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/login', function () {
    return view('layouts/login');
});

// <!-- 
// | =================================================================================
// |                      Admin Routes
// | =================================================================================
// -->

// Admin Pages
Route::get('/admin/home', [AdminController::class, 'adhome']);
Route::get('/admin/employee', [AdminController::class, 'ademployee']);
Route::get('/admin/employee-acc', [AdminController::class, 'ademployeeacc']);
Route::get('/admin/assignment', [AdminController::class, 'adassignment']);
Route::get('/admin/settings', [AdminController::class, 'adsettings']);
Route::get('/admin/change-pass', [AdminController::class, 'adchangepass']);
Route::get('/admin/security-questions', [AdminController::class, 'adsecquestion']);

// Employee Routes
Route::get('/admin/allemployee', [AdminController::class, 'allmployee']);
Route::get('/admin/employeedatatables', [AdminController::class, 'employeeDt']);
Route::post('/admin/addemployee', [AdminController::class, 'addemployee']);
Route::get('/admin/findemployee/{id}', [AdminController::class, 'findemployee']);
Route::put('/admin/updateemployee/{id}', [AdminController::class, 'updateemployee']);
Route::put('/admin/updatestatusemployee/{id}', [AdminController::class, 'updatestatusemployee']);


