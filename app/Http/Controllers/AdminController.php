<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Employee;
use DataTables;
use DB;

class AdminController extends Controller
{
    public function adhome() {
        return view('layouts/admin/plugins', ['active' => '/admin/home']);
    }

    public function ademployee() {
        
        return view('layouts/admin/employee', ['active' => '/admin/employee']);
    }

    public function allmployee() {
        //get all the employee for the datatables
        $employees = Employee::all();
        return response()->json([
            'employees'=>$employees,
        ]);
    }

    public function employeeDt(Request $request) {

        
        //selecting the column that needs to be input into the datatables
        // $query = Employee::select(
        //     'id',
        //     'first_name',
        //     'middle_name',
        //     'last_name',
        //     'sex',
        //     'birthday',
        //     'status_active'
        // );
 
        //checking the request of ajax
        if(request()->ajax()){
            //checking if the filter status is not empty
            if(!empty($request->filter_status)){
                //if not getting the data of the search filter
                $data = DB::table('employee')
                    ->select('id', 'first_name', 'middle_name', 'last_name', 'sex', 'birthday', 'status_active')
                    ->where('status_active', $request->input('filter_status'))
                    ->where('sex', $request->input('filter_gender'))
                    ->get();
            } else {
                //if the filter status is not empty getting all the employee list
                $data = DB::table('employee')
                        ->select('id', 'first_name', 'middle_name', 'last_name', 'sex', 'birthday', 'status_active')
                        ->get();
            }
            //returning all the employee list
            return DataTables()->of($data)->make(true);
        }
        // if($query){
        //     return DataTables($query)->make(true);
        // }
    }

    public function addemployee(Request $request) {
        //for validation
        $validation = Validator::make($request->all(), [
            'fname'=>'required|max:191',
            'mname'=>'max:191',
            'lname'=>'required|max:191',
            'bday'=>'required',
            'sex'=>'required|max:191',
            'address'=>'required|max:191',
            'contnum'=>'required|max:11|min:11',
        ],[
            'fname.required'=>'Please input your first name',
            'lname.required'=>'Please input your last name',
            'bday.required'=>'Please select your birthday',
            'sex.required'=>'Please select your sex',
            'address.required'=>'Please input your address',
            'contnum.required'=>'Please input your contact number',
            'contnum.max'=>'Maximum input must be 11 numbers',
            'contnum.min'=>'Minimum input must be 11 numbers',
        ]);
        // if the validation is not followed
        if($validation->fails()) {
            return response()->json([
                'status'=>400,
                'error'=>$validation->messages(),
            ]);
        } else {
            //else it will create the employee
            $employee = new Employee;
            $employee->first_name = $request->input('fname');
            $employee->middle_name = $request->input('mname');
            $employee->last_name = $request->input('lname');
            $employee->birthday = $request->input('bday');
            $employee->sex = $request->input('sex');
            $employee->address = $request->input('address');
            $employee->contact_num = $request->input('contnum');
            $employee->save();

            // getting an id after saving $employee->$id
            return response()->json([
                'status'=>200,
                'message'=>'Employee Successfully Registered',
            ]);
        }
        
    }

    public function findemployee($id) {
        //find one of the
        $employee = Employee::find($id);
        if($employee){
            return response()->json([
                'status' => 200,
                'employee' => $employee,
            ]);
        } else {
            return response()->json([
                'status' => 404,
                'message' => 'Employee not found.'
            ]);
        }
    }

    public function updateemployee(Request $request, $id) {
        //for validation
        $validation = Validator::make($request->all(), [
            'fname'=>'required|max:191',
            'mname'=>'max:191',
            'lname'=>'required|max:191',
            'bday'=>'required',
            'sex'=>'required|max:191',
            'address'=>'required|max:191',
            'contnum'=>'required|max:11|min:11',
        ],[
            'fname.required'=>'Please input your first name',
            'lname.required'=>'Please input your last name',
            'bday.required'=>'Please select your birthday',
            'sex.required'=>'Please select your sex',
            'address.required'=>'Please input your address',
            'contnum.required'=>'Please input your contact number',
            'contnum.max'=>'Maximum input must be 11 numbers',
            'contnum.min'=>'Minimum input must be 11 numbers',
        ]);
        //if the validation is not follow
        if($validation->fails()) {
            return response()->json([
                'status'=>400,
                'error'=>$validation->messages(),
            ]);
        } else {
            //it will update the employee details
            $employee = Employee::find($id);
            if($employee){
                $employee->first_name = $request->input('fname');
                $employee->middle_name = $request->input('mname');
                $employee->last_name = $request->input('lname');
                $employee->birthday = $request->input('bday');
                $employee->sex = $request->input('sex');
                $employee->address = $request->input('address');
                $employee->contact_num = $request->input('contnum');
                $employee->update();
                return response()->json([
                    'status'=>200,
                    'message'=>'Employee Successfully Updated',
                ]);
            } else {
                return response()->json([
                    'status' => 404,
                    'message' => 'Employee not found.'
                ]);
            }
            
        }
    }

    public function updatestatusemployee(Request $request, $id) {
        $validation = Validator::make($request->all(), [
            'status'=>'required',
        ],[
            'status.required'=>'Please select status'
        ]);
        if($validation->fails()) {
            return response()->json([
                'status'=>400,
                'error'=>$validation->messages(),
            ]);
        } else {
            $employee = Employee::find($id);
            if($employee){
                $employee->status_active = $request->input('status');
                $employee->update();
                return response()->json([
                    'status'=>200,
                    'message'=>'Employee Status Successfully Updated',
                ]);
            } else {
                return response()->json([
                    'status' => 404,
                    'message' => 'Employee not found.'
                ]);
            }
        }
    }

    public function ademployeeacc() {
        return view('layouts/admin/employeeacc', ['active' => '/admin/employee']);
    }

    public function adassignment() {
        return view('layouts/admin/assignment', ['active' => '/admin/assignment']);
    }

    public function adsettings() {
        return view('layouts/admin/userprofile', ['active' => '/admin/settings']);
    }

    public function adchangepass() {
        return view('layouts/admin/changepass', ['active' => '/admin/settings']);
    }

    public function adsecquestion() {
        return view('layouts/admin/secquestion', ['active' => '/admin/settings']);
    }
}
