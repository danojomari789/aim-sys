<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>AIM SYS | Log-in</title>

  <!-- Plugins -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css')}}">

</head>
<!-- Alertbox -->
<div id="loginAlertContainer"></div>


<body class="hold-transition login-page">

    <div class="login-box">
      <div class="login-logo">
          <h1>AIM SYS</h1>
      </div>

      <div class="card shadow-none">
          <div class="card-body login-card-body">
              <p class="login-box-msg">Log in to start your session</p>

              <form id="AdminAuthForm">

                  <!-- Email -->
                  <div class="form-group">
                      <label for="email">Email</label>
                      <div class="input-group">
                          <input type="emailLogin" id="emailLogin" class="form-control" name="email" placeholder="Email">
                          <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-envelope"></span>
                          </div>
                          </div>
                      </div>
                  </div>

                  <!-- Password -->
                  <div class="form-group">
                      <div class="d-flex align-items-center justify-content-between">
                          <label for="password">Password</label>
                          <a class="mb-3" href="#">I forgot my password</a>
                      </div>
                      <div class="input-group">
                          <input type="password" class="form-control" id="passwordLogin" name="passwordLogin" placeholder="Password">
                          <div class="input-group-append">
                              <div class="input-group-text">
                                  <span class="fas fa-lock"></span>
                              </div>
                          </div>
                      </div>
                  </div>

                  <!-- User Actions -->
                  <button type="submit" class="btn btn-primary btn-block">
                      <span>Log in</span>
                      <i class="fas fa-sign-in-alt ml-1"></i>
                  </button>
              </form>
          </div>
      </div>
   </div>

    <!-- BLOCK FOR PLUGINS JAVASCRIPT -->
    <script src="{{ asset('plugins/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
    <script src="{{ asset('dist/js/adminlte.min.js')}}"></script>
    <script src="{{ asset('plugins/sweetalert2/sweetalert2.min.js')}}"></script>
    <script src="{{ asset('plugins/toastr/toastr.min.js')}}"></script>

    {{-- <!-- BLOCK FOR EXTRA SCRIPTS -->
    {%block scripts%}
    {%endblock%} --}}

  

</body>
</html>