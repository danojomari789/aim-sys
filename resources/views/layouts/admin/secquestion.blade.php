@extends('layouts.admin.home')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">User Profile</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Security Questions</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->

        <div class="row">
            <div class="col-md-3 col-sm-6 col-12">
                <div class="card">
                    <div class="card-header text-center">
                      Menu
                    </div>
                    <div class="card-body">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                            <a href="{{ url('/admin/settings') }}" class="nav-link">
                                <i class="nav-icon fas fa-user"></i>
                                <p>
                                    Profile
                                </p>
                            </a>
                            <a href="{{ url('/admin/change-pass') }}" class="nav-link">
                                <i class="nav-icon fas fa-key"></i>
                                <p>
                                    Change Password
                                </p>
                            </a>
                            <a href="{{ url('/admin/security-questions') }}" class="nav-link active">
                                <i class="nav-icon fas fa-lock"></i>
                                <p>
                                    Security Questions
                                </p>
                            </a>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-sm-6 col-12">
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <h3 class="profile-username">Security Questions</h3>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <label for="guardianinputAdd">Question #1</label>
                                        <select 
                                            class="form-control" 
                                            style="width: 100%;"
                                            id="sexinputAdd"
                                            name="sexinputAdd"
                                            required
                                        >
                                            <option selected="selected" disabled>Select Questions*</option>
                                            <option value="1">What is the name of your favorite pet?</option>
                                            <option value="2">What is your mother's maiden name?</option>
                                            <option value="3">What high school did you attend?</option>
                                            <option value="4">What is the name of your first school?</option>
                                            <option value="5">What was your favorite food as a child?</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <label for="guardianinputAdd">Answer #1</label>
                                        <input 
                                            class="form-control"
                                            type="text"
                                            id="firstnameinputAdd"
                                            name="firstnameinputAdd"
                                            placeholder="Enter Your Password"
                                        >
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <label for="guardianinputAdd">Question #2</label>
                                        <select 
                                            class="form-control" 
                                            style="width: 100%;"
                                            id="sexinputAdd"
                                            name="sexinputAdd"
                                            required
                                        >
                                            <option selected="selected" disabled>Select Questions*</option>
                                            <option value="1">What is the name of your favorite pet?</option>
                                            <option value="2">What is your mother's maiden name?</option>
                                            <option value="3">What high school did you attend?</option>
                                            <option value="4">What is the name of your first school?</option>
                                            <option value="5">What was your favorite food as a child?</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <label for="guardianinputAdd">Answer #2</label>
                                        <input 
                                            class="form-control"
                                            type="text"
                                            id="firstnameinputAdd"
                                            name="firstnameinputAdd"
                                            placeholder="Enter Your Password"
                                        >
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <label for="guardianinputAdd">Question #3</label>
                                        <select 
                                            class="form-control" 
                                            style="width: 100%;"
                                            id="sexinputAdd"
                                            name="sexinputAdd"
                                            required
                                        >
                                            <option selected="selected" disabled>Select Questions*</option>
                                            <option value="1">What is the name of your favorite pet?</option>
                                            <option value="2">What is your mother's maiden name?</option>
                                            <option value="3">What high school did you attend?</option>
                                            <option value="4">What is the name of your first school?</option>
                                            <option value="5">What was your favorite food as a child?</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <label for="guardianinputAdd">Answer #3</label>
                                        <input 
                                            class="form-control"
                                            type="text"
                                            id="firstnameinputAdd"
                                            name="firstnameinputAdd"
                                            placeholder="Enter Your Password"
                                        >
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <label for="guardianinputAdd">Question #4</label>
                                        <select 
                                            class="form-control" 
                                            style="width: 100%;"
                                            id="sexinputAdd"
                                            name="sexinputAdd"
                                            required
                                        >
                                            <option selected="selected" disabled>Select Questions*</option>
                                            <option value="1">What is the name of your favorite pet?</option>
                                            <option value="2">What is your mother's maiden name?</option>
                                            <option value="3">What high school did you attend?</option>
                                            <option value="4">What is the name of your first school?</option>
                                            <option value="5">What was your favorite food as a child?</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <label for="guardianinputAdd">Answer #4</label>
                                        <input 
                                            class="form-control"
                                            type="text"
                                            id="firstnameinputAdd"
                                            name="firstnameinputAdd"
                                            placeholder="Enter Your Password"
                                        >
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <label for="guardianinputAdd">Question #5</label>
                                        <select 
                                            class="form-control" 
                                            style="width: 100%;"
                                            id="sexinputAdd"
                                            name="sexinputAdd"
                                            required
                                        >
                                            <option selected="selected" disabled>Select Questions*</option>
                                            <option value="1">What is the name of your favorite pet?</option>
                                            <option value="2">What is your mother's maiden name?</option>
                                            <option value="3">What high school did you attend?</option>
                                            <option value="4">What is the name of your first school?</option>
                                            <option value="5">What was your favorite food as a child?</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <label for="guardianinputAdd">Answer #5</label>
                                        <input 
                                            class="form-control"
                                            type="text"
                                            id="firstnameinputAdd"
                                            name="firstnameinputAdd"
                                            placeholder="Enter Your Password"
                                        >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <a href="#" class="btn btn-primary btn-block"><b>Save</b></a>   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div><!-- /.container-fluid -->
</div>
@endsection


@section('scripts')
<script>
    $(function () {
        $("#example1").DataTable({
            "responsive": true, "lengthChange": false, "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
        });

    });

    // $(function () {
    //     $('#sexinputAdd').select2();

    //     $(".select").select2({
    //         minimumResultsForSearch: Infinity
    //     });
    // });
</script>
@endsection


@section('plugincss')

<link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/jqvmap/jqvmap.min.css') }}">
<link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.csss') }}">
<link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">

<link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">

@endsection

@section('javascript')

<script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('plugins/sparklines/sparkline.js') }}"></script>
<script src="{{ asset('plugins/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
<script src="{{ asset('plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<script src="{{ asset('dist/js/adminlte.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>

<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>

@endsection