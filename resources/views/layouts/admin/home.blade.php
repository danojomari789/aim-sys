<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AIM SYS</title>

    {{-- X-CSRF-TOKEN laravel --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- IonIcons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/css/menu.css') }}">

    @yield('plugincss')
</head>
<!--
`body` tag options:

  Apply one or more of the following classes to to the body tag
  to get the desired effect

  * sidebar-collapse
  * sidebar-mini
-->
<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="index3.html" class="nav-link">Home</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="#" class="nav-link">Contact</a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
            <!-- Navbar Search -->
                <li class="nav-item">
                    <a class="nav-link" data-widget="navbar-search" href="#" role="button">
                        <i class="fas fa-search"></i>
                    </a>
                    <div class="navbar-search-block">
                        <form class="form-inline">
                            <div class="input-group input-group-sm">
                                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                            <div class="input-group-append">
                                <button class="btn btn-navbar" type="submit">
                                    <i class="fas fa-search"></i>
                                </button>
                                <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                            </div>
                        </form>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a href="#" class="nav-link" data-toggle="dropdown" role="button">
                        <i class="fas fa-user-circle"></i>
                        <!-- <span class="badge badge-warning navbar-badge">15</span> -->
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a
                            href="#"
                            role="button" 
                            class="dropdown-item d-flex align-items-center" 
                            data-toggle="modal" 
                            data-target="#logoutModal"
                        >
                            <div class="mr-3" style="width: 1.25rem">
                                <i class="fas fa-sign-out-alt"></i>
                            </div>
                            <span>Log out</span>
                        </a>
                    </div>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="index3.html" class="brand-link">
                <img src="{{ asset('dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">AIM SYS</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <a href="#" class="d-block">Alexander Pierce</a>
                </div>
            </div>


            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                    with font-awesome or any other icon font library -->
                    <li class="nav-item menu-open">
                        @if ( $active == '/admin/home')
                            <a href="{{ url('/admin/home') }}" class="nav-link active">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    Dashboard
                                </p>
                            </a>
                            @else
                                <a href="{{ url('/admin/home') }}" class="nav-link">
                                    <i class="nav-icon fas fa-tachometer-alt"></i>
                                    <p>
                                        Dashboard
                                    </p>
                                </a>
                        @endif
                    </li>
                    <li class="nav-header">EXAMPLES</li>
                    <li class="nav-item">
                        @if ( $active == '/admin/employee')
                            <a href="{{ url('/admin/employee') }}" class="nav-link active">
                                <i class="nav-icon fas fa-users"></i>
                                <p>
                                    Employee
                                </p>
                            </a>
                            @else
                                <a href="{{ url('/admin/employee') }}" class="nav-link">
                                    <i class="nav-icon fas fa-users"></i>
                                    <p>
                                        Employee
                                    </p>
                                </a>
                        @endif
                    </li>
                    <li class="nav-item">
                        @if ( $active == '/admin/assignment')
                            <a href="{{ url('/admin/assignment') }}" class="nav-link active">
                                <i class="nav-icon fas fa-clipboard-check"></i>
                                <p>
                                    Assignment
                                </p>
                            </a>
                            @else
                                <a href="{{ url('/admin/assignment') }}" class="nav-link">
                                    <i class="nav-icon fas fa-clipboard-check"></i>
                                    <p>
                                        Assignment
                                    </p>
                                </a>
                        @endif
                    </li>
                    <li class="nav-item">
                        @if ( $active == '/admin/settings')
                            <a href="{{ url('/admin/settings') }}" class="nav-link active">
                                <i class="nav-icon fas fa-cog"></i>
                                <p>
                                    Settings
                                </p>
                            </a>
                            @else
                                <a href="{{ url('/admin/settings') }}" class="nav-link">
                                    <i class="nav-icon fas fa-cog"></i>
                                    <p>
                                        Settings
                                    </p>
                                </a>
                        @endif
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('content')
        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong>
            All rights reserved.
            <div class="float-right d-none d-sm-inline-block">
            <b>Version</b> 3.1.0
            </div>
        </footer>

    <!-- 
    | =================================================================================
    |                      LOGOUT MODAL (For demo purposes)
    | =================================================================================
    -->
    <div class="modal" id="logoutModal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
  
                <div class="modal-header">
                    <h4 class="modal-title">
                        <i class="fas fa-sign-out-alt text-secondary mr-2"></i>
                        <span>Log out</span>
                    </h4>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" id="closeLogoutModalBtn">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                </div>
  
                <div class="modal-body">
                    <p>Are you sure you want to logout?</p>
                </div>
                
                <div class="modal-footer">
                    <button 
                        type="button" 
                        class="btn btn-default" 
                        data-dismiss="modal"
                        id="cancelLogoutBtn"
                    >Cancel</button>
                    <button type="button" class="btn btn-danger" id="logoutBtn" onclick="LogoutUser()">
                        <span>Log out</span>
                        <i class="fas fa-sign-out-alt ml-1"></i>
                    </button>
                </div>
            </div>
        </div>
      </div>
    </div>

    
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->

    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- AdminLTE -->
    <script src="{{ asset('dist/js/adminlte.js') }}"></script>

    <!-- OPTIONAL SCRIPTS -->
    <script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('dist/js/demo.js') }}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{ asset('dist/js/pages/dashboard3.js') }}"></script>
    <script src="{{ asset('dist/js/menu.js') }}"></script>
    @yield('javascript')

    {{-- Scripts --}}
    @yield('scripts')
</body>
</html>
