@extends('layouts.admin.home')
@include('layouts.admin.modal.employeeModal')

@section('content')


<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Employee Management</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ url('/admin/employee') }}">Employee</a></li>
                    <li class="breadcrumb-item active">Dashboard v3</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->

        {{-- @foreach($employee as $employees)
          <div>
            {{ $employees->first_name }} - {{ $employees->middle_name}} - {{ $employees->last_name}} - {{ $employees->sex}}
          </div>
        @endforeach --}}
        {{-- <div id="success_massage">

        </div> --}}
        
        <div class="row">
          <div class="col-md-3 col-sm-6 col-12">
            <div class="card">
              <div class="card-header text-center">
                Menu
              </div>
              <div class="card-body">
                  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                      <a href="{{ url('/admin/employee') }}" class="nav-link active">
                          <i class="nav-icon fas fa-user"></i>
                          <p>
                              Employee
                          </p>
                      </a>
                      <a href="{{ url('/admin/employee-acc') }}" class="nav-link">
                        <i class="nav-icon fas fa-user-circle"></i>
                        <p>
                            Employee Account
                        </p>
                      </a>
                      <a href="{{ url('/admin/change-pass') }}" class="nav-link">
                          <i class="nav-icon fas fa-map-marker"></i>
                          <p>
                              Position
                          </p>
                      </a>
                  </ul>
              </div>
            </div>
          </div>
          <div class="col-md-9 col-sm-6 col-12">
            <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Employee</h3>
                  <div class="col-md-12 text-right">
                    <a type="button" class="btn btn bg-gradient-success" data-toggle="modal" data-target="#addemployee">
                      <i class="fas fa-plus"></i>
                      Add Employee
                    </a>
                  </div>
                  <div class="row">
                    <div class="col-md-2 form-group"></div>
                    <div class="col-md-4 form-group">
                      <div class="form-group">
                        <label for="filter_status">Search Status</label>
                        <select 
                            class="form-control" 
                            style="width: 100%;"
                            id="filter_status"
                            name="filter_status"
                        >
                            <option value="">Select Status</option>
                            <option value="Active">Active</option>
                            <option value="In-active">In-active</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4 form-group">
                      <div class="form-group">
                        <label for="filter_gender">Search Gender</label>
                        <select 
                            class="form-control" 
                            style="width: 100%;"
                            id="filter_gender"
                            name="filter_gender"
                        >
                            <option value="">Select Sex</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-4 form-group"></div>
                    <div class="col-md-2 form-group">
                      <button type="button" class="btn btn-info form-control" name="filter" id="filter">
                        Filter
                      </button>
                    </div>
                    <div class="col-md-2 form-group">
                      <button type="button" class="btn btn-default form-control" name="reset" id="reset">
                        Reset
                      </button>
                    </div>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="employeetable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Employee Name</th>
                      <th>Gender</th>
                      <th>Age</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
            </div>
          </div>
        </div>
        
    </div><!-- /.container-fluid -->
</div>
@endsection


@section('scripts')
<script type="text/javascript">
  
  $(document).ready(function (){

    //Adding/ Creating Employee
    $(document).on('click','.add_employee', function (e) {
      e.preventDefault();
     //getting the data or the input
      var data = {
        'fname': $('#firstnameinputAdd').val(),
        'mname': $('#middlenameinputAdd').val(),
        'lname': $('#lastnameinputAdd').val(),
        'bday': $('#birthdayinputAdd').val(),
        'sex': $('#sexinputAdd').val(),
        'address': $('#addressinputAdd').val(),
        'contnum': $('#contnuminputAdd').val()
      }
      // console.log(data)

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });


      $.ajax({
        type:'POST',
        url: '/admin/addemployee',
        data: data,
        dataType:'json',
        success: function (result) {
          // console.log(result.error.fname);
          //for validation if it is not followed
          if(result.status == 400) {
            // $('#save_errlist').html("");
            // $('#save_errlist').addClass("alert alert-danger");
            $.each(result.error.fname, function (key, err_values) {
              $('#err_fname').html("");
              $('#err_fname').append('<span class="text-danger">'+err_values+'</span>')
            });
            $.each(result.error.lname, function (key, err_values) {
              $('#err_lname').html("");
              $('#err_lname').append('<span class="text-danger">'+err_values+'</span>')
            });
            $.each(result.error.mname, function (key, err_values) {
              $('#err_mname').html("");
              $('#err_mname').append('<span class="text-danger">'+err_values+'</span>')
            });
            $.each(result.error.bday, function (key, err_values) {
              $('#err_bday').html("");
              $('#err_bday').append('<span class="text-danger">'+err_values+'</span>')
            });
            $.each(result.error.sex, function (key, err_values) {
              $('#err_sex').html("");
              $('#err_sex').append('<span class="text-danger">'+err_values+'</span>')
            });
            $.each(result.error.contnum, function (key, err_values) {
              $('#err_contnum').html("");
              $('#err_contnum').append('<span class="text-danger">'+err_values+'</span>')
            });
            $.each(result.error.address, function (key, err_values) {
              $('#err_address').html("");
              $('#err_address').append('<span class="text-danger">'+err_values+'</span>')
            });
          } else {
            //creating the employee
            // $('#success_massage').addClass('alert alert-success')
            // $('#success_massage').text(result.message)
            toastr.success(result.message)
            $('#addemployee').modal('hide');
            $('#addemployee').find('input').val("");
            $('#err_fname').html("");
            $('#err_lname').html("");
            $('#err_mname').html("");
            $('#err_bday').html("");
            $('#err_sex').html("");
            $('#err_contnum').html("");
            $('#err_address').html("");
            EmployeeDT();
          }
        }
      });

    });

    //to get all the employee and put it to the datatable
    // function EmployeeDT() {
    //   $.ajax({
    //     type:'GET',
    //     url:'/admin/allemployee',
    //     dataType:'json',
    //     success: function(result) {
    //       console.log(result)
    //       $('tbody').html("");
    //       $.each(result.employees, function(key, item) {
    //         //putting the data to the table
    //         $('tbody').append('<tr>\
    //           <td><i class="fas fa-user"></i>&nbsp;&nbsp;'+item.last_name+' '+item.first_name+','+item.middle_name+'</td>\
    //           <td>'+item.sex+'</td>\
    //           <td>'+item.birthday+'</td>\
    //           <td>'+item.contact_num+'</td>\
    //           <td>\
    //             <button type="button" value="'+item.id+'" class="view-employee btn btn-primary">\
    //               <i class="fas fa-eye"></i></button>\
    //             <button type="button" value="'+item.id+'" class="edit-employee btn btn-info">\
    //               <i class="fas fa-edit"></i></button>\
    //             <button type="button" value="'+item.id+'" class="inactive-employee btn btn-danger">\
    //               <i class="fas fa-user-alt-slash"></i></i></button>\
    //             </td>\
    //         </tr>')
    //       })
    //     }
    //   })
    // }

    // EmployeeDT();

    // fill_datatable()

    // function fill_datatable(filter_name = '', filter_gender = '') {
    //   var table = $('#employeetable').DataTable({
    //     serverSide: true,
    //     responsive: false,
    //     // processing: true,
    //     buttons:[
    //         {extend: 'excel', text: 'Save to Excel File'}
    //     ],
    //     ajax: "{{ url('/admin/employeedatatables')}}",
    //     data: { filter_name: filter_name, filter_gender: filter_gender },
    //     columns: [
    //       {data: "id", visible:false, },
    //       {
    //         data: null,
    //         render: (data) => {

    //           var middleName = data.middle_name
    //           middleName = (middleName == null || middleName == '') ? '' : ' ' + middleName;
    //           const fullName = data.last_name + ', ' + data.first_name + middleName;

    //           return `
    //             <a>
    //               <i class="fas fa-user"></i>
    //               ${ fullName }
    //             </a>
                
    //           `
    //         }
    //       },
    //       {
    //         data: null,
    //         render: (data) => {
    //           const sex = data.sex

    //           if (sex == 'Male') {
    //             icon = '<i class="fas fa-mars text-blue"></i>'
    //           } else {
    //             icon = '<i class="fas fa-venus text-danger"></i>'
    //           }

    //           return `
    //               <a>
    //                 ${ icon }
    //                 ${ sex }
    //               </a>
    //             `
    //           }
    //       },
    //       {
    //         data: null,
    //         render: (data) => {

    //           var Bdate = data.birthday
    //           var Bday = +new Date(Bdate);
    //           var bday = ((Date.now() - Bday) / (31557600000));
    //           var intvalue = Math.floor( bday );

    //           return `
    //               ${ intvalue }
    //             `
    //           }
    //       },
    //       {
    //         data: null,
    //         render: (data) => {
    //           const Status = data.status_active

    //           if (Status == 'Active') {
    //             icon = '<i class="fas fa-user-check"></i>'
    //           } else {
    //             icon = '<i class="fas fa-user-times"></i>'
    //           }

    //           return `
    //               <a>
    //                 ${ icon }
    //                 ${ Status }
    //               </a>
    //             `
    //           }
    //       },
    //       {
    //         data: null,
    //         class: 'text-center',
    //         render: (data) => {
    //           return `
    //             <button 
    //               type="button" 
    //               class="view-employee btn btn-primary"
    //               onclick="ViewEmployeeDetails('${ data.id }')"
    //             >
    //               <i class="fas fa-eye"></i>
    //             </button>
    //             <button 
    //               type="button" 
    //               class="edit-employee btn btn-info"
    //               onclick="EditEmployeeDetails('${ data.id }')"
    //             >
    //               <i class="fas fa-edit"></i>
    //             </button>
    //             <button 
    //               type="button" 
    //               class="inactive-employee btn btn-danger"
    //               onclick="InactiveEmployee('${ data.id }')"
    //             >
    //               <i class="fas fa-user-alt-slash"></i></i>
    //             </button>
    //           `
    //         }
    //       }
    //     ]
    //   })
    // }

    EmployeeDT = (filter_gender = '', filter_status = '') => {
      $('#employeetable').dataTable().fnClearTable()
      $('#employeetable').dataTable().fnDraw()
      $('#employeetable').dataTable().fnDestroy()
      var table = $('#employeetable').DataTable({
        serverSide: true,
        responsive: false,
        // processing: true,
        buttons:[
            {extend: 'excel', text: 'Save to Excel File'}
        ],
        ajax: {
          url: "{{ url('/admin/employeedatatables')}}",
          //for filtering get the value
          data: { filter_gender:filter_gender, filter_status:filter_status },
        },
        columns: [
          {data: "id", visible:false, name:'id' },
          {
            data: null,
            name:'last_name',
            render: (data) => {

              var middleName = data.middle_name
              middleName = (middleName == null || middleName == '') ? '' : ' ' + middleName;
              const fullName = data.last_name + ', ' + data.first_name + middleName;

              return `
                <a>
                  <i class="fas fa-user"></i>
                  ${ fullName }
                </a>
                
              `
            }
          },
          {
            data: null,
            name:'sex',
            render: (data) => {
              const sex = data.sex

              if (sex == 'Male') {
                icon = '<i class="fas fa-mars text-blue"></i>'
              } else {
                icon = '<i class="fas fa-venus text-danger"></i>'
              }

              return `
                  <a>
                    ${ icon }
                    ${ sex }
                  </a>
                `
              }
          },
          {
            data: null,
            name:'birthday',
            render: (data) => {

              var Bdate = data.birthday
              var Bday = +new Date(Bdate);
              var bday = ((Date.now() - Bday) / (31557600000));
              var intvalue = Math.floor( bday );

              return `
                  ${ intvalue }
                `
              }
          },
          {
            data: null,
            name:'status_active',
            render: (data) => {
              const Status = data.status_active

              if (Status == 'Active') {
                icon = '<i class="fas fa-user-check"></i>'
              } else {
                icon = '<i class="fas fa-user-times"></i>'
              }

              return `
                  <a>
                    ${ icon }
                    ${ Status }
                  </a>
                `
              }
          },
          {
            data: null,
            class: 'text-center',
            render: (data) => {
              return `
                <button 
                  type="button" 
                  class="view-employee btn btn-primary"
                  onclick="ViewEmployeeDetails('${ data.id }')"
                >
                  <i class="fas fa-eye"></i>
                </button>
                <button 
                  type="button" 
                  class="edit-employee btn btn-info"
                  onclick="EditEmployeeDetails('${ data.id }')"
                >
                  <i class="fas fa-edit"></i>
                </button>
                <button 
                  type="button" 
                  class="inactive-employee btn btn-danger"
                  onclick="InactiveEmployee('${ data.id }')"
                >
                  <i class="fas fa-user-alt-slash"></i></i>
                </button>
              `
            }
          }
        ]
      })
    }

    EmployeeDT();

    //if the user click filter
    $('#filter').click(function(e) {
        e.preventDefault()
        //getting the data to throw to the ajax
        var filter_gender = $('#filter_gender').val();
        var filter_status = $('#filter_status').val();
        //checking if the gender and status has a value
        if(filter_gender != '' && filter_status != '') {
          $('#employeetable').DataTable().destroy();
          EmployeeDT(filter_gender, filter_status);
        } else {
          toastr.error('Please input both filter to search');
        }
    });

    //resetting the input of the filters
    $('#reset').click(function() {
      $('#filter_gender').val('');
      $('#filter_status').val('');
      $('#employeetable').DataTable().destroy();
      EmployeeDT();
    })

    //viewing employee details
    ViewEmployeeDetails = (id) => {
      //getting the id
      const employee_id = id
      console.log(id)
      // showing the modal
      $('#viewemployee').modal('show');
      //ajax for getting the data
      $.ajax({
        type: 'GET',
        url: '/admin/findemployee/'+employee_id,
        success: function(result) {
          // console.log(result)
          //checking if the data is get in the db
          if(result) {
              //putting the data to the inputs
              var middleName = result.employee.middle_name
              middleName = (middleName == null || middleName == '') ? '' : ' ' + middleName;
              const fullName = result.employee.last_name + ', ' + result.employee.first_name + middleName;
              $("#firstnameinputView").html(fullName);

              $("#profilenameView").html(fullName);

              const seX = () => {
                if(result.employee.sex == 'Male'){
                  badge = 'primary'
                  icon = 'mars'
                } else {
                  badge = 'info'
                  icon = 'venus'
                }

                return `

                  <span class="badge badge-${badge}">
                    <i class="fas fa-${icon} mr-1"></i>
                    ${result.employee.sex}
                  </span>
                  
                `
              }
              $('#sexinputView').html(seX());

              var Bdate = result.employee.birthday
              var Bday = +new Date(Bdate);
              var bday = ((Date.now() - Bday) / (31557600000));
              const intvalue = Math.floor( bday );
              $('#ageinputView').html(intvalue);

              const birthday = moment(result.employee.birthday).format('MMM. D, YYYY')
              $('#bdayinputView').html(birthday);
              $('#contactnuminputView').html('0'+result.employee.contact_num);
              $('#contactnuminputViews').html('0'+result.employee.contact_num);
              $('#addressinputView').html(result.employee.address);

              const Status = () => {
                if(result.employee.status_active == 'Active'){
                  badge = 'primary'
                  icon = 'user-check'
                } else {
                  badge = 'danger'
                  icon = 'user-times'
                }

                return `

                  <span class="badge badge-${badge}">
                    <i class="fas fa-${icon} mr-1"></i>
                    ${result.employee.status_active}
                  </span>
                  
                `
              }
              $('#statusinputView').html(Status());
            } else {
              //if the data is not get give a message
              toastr.error(result.message);
           }
        }
      })
    }

    InactiveEmployee = (id) => {
      const employee_id = id
      // console.log(employee_id+'asgasgagaw')
      //showing the modal
      $('#inactiveemployee').modal('show');
      //getting the data of the status
      $.ajax({
        type: 'GET',
        url: '/admin/findemployee/'+employee_id,
        success: function(result) {
          console.log(result.employee.status_active)
          if(result.status == 404) {
              toastr.error(result.message)
            } else {
             //putting the data to the inputs
              $('#statusinputEdit').val(result.employee.status_active);

              $(document).on('click', '.updatestatus_employee', function(e) {
                e.preventDefault();

                var data = {
                  'status': $('#statusinputEdit').val()
                }
                //laravel token
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                
                //ajax for updating the employee status
                $.ajax({
                  type: 'PUT',
                  url: '/admin/updatestatusemployee/'+employee_id,
                  data: data,
                  dataType: 'json',
                  success: function(result) {
                    //checking if the validation is followed
                    if(result.status == 400) {
                      $.each(result.error.status, function (key, err_values) {
                        $('#editerr_status').html("");
                        $('#editerr_status').append('<span class="text-danger">'+err_values+'</span>')
                      });
                      // checking if the employee is search
                    } else if (result.status == 404) {
                      toastr.error(result.message)
                    } else {
                      //updating the employee details
                      toastr.success(result.message)
                      $('#inactiveemployee').modal('hide');
                      $('#inactiveemployee').find('input').val("");
                      EmployeeDT();
                      setInterval(function(){
                        window.location.replace('{{ url('/admin/employee')}}')
                      }, 1000)
                    }
                  }
                })
              })
           }
        }
      })
    }

    //editing employee details
    EditEmployeeDetails = (id) => {
      const employee_id = id
      // console.log(id)
      //showing the modal
      $('#updateemployee').modal('show');
      //getting the data
      $.ajax({
        type: 'GET',
        url: '/admin/findemployee/'+employee_id,
        success: function(result) {
          console.log(result)
          if(result.status == 404) {
              toastr.error(result.message)
           } else {
             //putting the data to the inputs
              $('#firstnameeditAdd').val(result.employee.first_name);
              $('#lastnameeditAdd').val(result.employee.last_name);
              $('#middlenameeditAdd').val(result.employee.middle_name);
              $('#birthdayeditAdd').val(result.employee.birthday);
              $('#sexeditAdd').val(result.employee.sex);
              $('#addresseditAdd').val(result.employee.address);
              $('#contnumeditAdd').val('0'+result.employee.contact_num);

              //updating the employee details
              $(document).on('click', '.update_employee', function(e) {
                e.preventDefault();
                //getting the data of the inputs
                var data = {
                  'fname': $('#firstnameeditAdd').val(),
                  'lname': $('#lastnameeditAdd').val(),
                  'mname': $('#middlenameeditAdd').val(),
                  'sex': $('#sexeditAdd').val(),
                  'bday': $('#birthdayeditAdd').val(),
                  'address': $('#addresseditAdd').val(),
                  'contnum': $('#contnumeditAdd').val(),

                };
                //laravel token
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                //updating the employee details
                $.ajax({
                  type: 'PUT',
                  url: '/admin/updateemployee/'+employee_id,
                  data: data,
                  dataType: 'json',
                  success: function(result) {
                    console.log(result)
                    //checking if the validation is followed
                    if(result.status == 400) {
                      $.each(result.error.fname, function (key, err_values) {
                        $('#editerr_fname').html("");
                        $('#editerr_fname').append('<span class="text-danger">'+err_values+'</span>')
                      });
                      $.each(result.error.lname, function (key, err_values) {
                        $('#editerr_lname').html("");
                        $('#editerr_lname').append('<span class="text-danger">'+err_values+'</span>')
                      });
                      $.each(result.error.mname, function (key, err_values) {
                        $('#editerr_mname').html("");
                        $('#editerr_mname').append('<span class="text-danger">'+err_values+'</span>')
                      });
                      $.each(result.error.bday, function (key, err_values) {
                        $('#editerr_bday').html("");
                        $('#editerr_bday').append('<span class="text-danger">'+err_values+'</span>')
                      });
                      $.each(result.error.sex, function (key, err_values) {
                        $('#editerr_sex').html("");
                        $('#editerr_sex').append('<span class="text-danger">'+err_values+'</span>')
                      });
                      $.each(result.error.contnum, function (key, err_values) {
                        $('#editerr_contnum').html("");
                        $('#editerr_contnum').append('<span class="text-danger">'+err_values+'</span>')
                      });
                      $.each(result.error.address, function (key, err_values) {
                        $('#editerr_address').html("");
                        $('#editerr_address').append('<span class="text-danger">'+err_values+'</span>')
                      });
                      //checking if the employee is search
                    } else if (result.status == 404) {
                      toastr.error(result.message)
                    } else {
                      //updating the employee details
                      toastr.success(result.message)
                      $('#updateemployee').modal('hide');
                      $('#updateemployee').find('input').val("");
                      EmployeeDT();
                      setInterval(function(){
                        window.location.replace('{{ url('/admin/employee')}}')
                      }, 1000)
                    }
                  }
                })
              })
           } 
        }
      })
    }


    

  });
</script>
<script>
  $('#addemployee').on('hidden.bs.modal', function (e) {
    $(this)
      .find("input,textarea,select")
        .val('')
        .end()
      .find("input[type=checkbox], input[type=radio]")
        .prop("checked", "")
        .end()
      .find('span')
        .html('')
        .end();
  })
    // $(function () {
    //     $("#employeetable").DataTable({
    //         "responsive": true, "lengthChange": false, "autoWidth": false,
    //         "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    //     }).buttons().container().appendTo('#employeetable_wrapper .col-md-6:eq(0)');
    //     $('#example2').DataTable({
    //         "paging": true,
    //         "lengthChange": false,
    //         "searching": false,
    //         "ordering": true,
    //         "info": true,
    //         "autoWidth": false,
    //         "responsive": true,
    //     });
    // });
</script>
@endsection


@section('plugincss')

<link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/jqvmap/jqvmap.min.css') }}">
<link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.min.css') }}">

<link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">

@endsection

@section('javascript')

<script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('plugins/sparklines/sparkline.js') }}"></script>
<script src="{{ asset('plugins/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
<script src="{{ asset('plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<script src="{{ asset('dist/js/adminlte.js') }}"></script>

<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>

@endsection