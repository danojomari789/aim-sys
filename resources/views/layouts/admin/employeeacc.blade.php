@extends('layouts.admin.home')
@include('layouts.admin.modal.accountModal')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Employee Account Management</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ url('/admin/employee-acc') }}">Employee Account</a></li>
                    <li class="breadcrumb-item active">Dashboard v3</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->

        {{-- @foreach($employee as $employees)
          <div>
            {{ $employees->first_name }} - {{ $employees->middle_name}} - {{ $employees->last_name}} - {{ $employees->sex}}
          </div>
        @endforeach --}}
        {{-- <div id="success_massage">

        </div> --}}
        
        <div class="row">
          <div class="col-md-3 col-sm-6 col-12">
            <div class="card">
              <div class="card-header text-center">
                Menu
              </div>
              <div class="card-body">
                  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                      <a href="{{ url('/admin/employee') }}" class="nav-link">
                          <i class="nav-icon fas fa-user"></i>
                          <p>
                              Employee
                          </p>
                      </a>
                      <a href="{{ url('/admin/emplyoee-acc') }}" class="nav-link active">
                        <i class="nav-icon fas fa-user-circle"></i>
                        <p>
                            Employee Account
                        </p>
                      </a>
                      <a href="{{ url('/admin/change-pass') }}" class="nav-link">
                          <i class="nav-icon fas fa-map-marker"></i>
                          <p>
                              Position
                          </p>
                      </a>
                  </ul>
              </div>
            </div>
          </div>
          <div class="col-md-9 col-sm-6 col-12">
            <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Employee Account</h3>
                  <div class="col-md-12 text-right">
                    <a type="button" class="btn btn bg-gradient-success" data-toggle="modal" data-target="#addemployee">
                      <i class="fas fa-plus"></i>
                      Add Account
                    </a>
                  </div>
                  <div class="row">
                    <div class="col-md-2 form-group"></div>
                    <div class="col-md-4 form-group">
                      <div class="form-group">
                        <label for="filter_status">Search Position</label>
                        <select 
                            class="form-control" 
                            style="width: 100%;"
                            id="filter_status"
                            name="filter_status"
                        >
                            <option value="">Select Position</option>
                            <option value="Active">Active</option>
                            <option value="In-active">In-active</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4 form-group">
                      <div class="form-group">
                        <label for="filter_gender">Search Status</label>
                        <select 
                            class="form-control" 
                            style="width: 100%;"
                            id="filter_gender"
                            name="filter_gender"
                        >
                            <option value="">Select Status</option>
                            <option value="Active">Active</option>
                            <option value="In-active">In-active</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-4 form-group"></div>
                    <div class="col-md-2 form-group">
                      <button type="button" class="btn btn-info form-control" name="filter" id="filter">
                        Filter
                      </button>
                    </div>
                    <div class="col-md-2 form-group">
                      <button type="button" class="btn btn-default form-control" name="reset" id="reset">
                        Reset
                      </button>
                    </div>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="employeetable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Employee Name</th>
                      <th>Gender</th>
                      <th>Email</th>
                      <th>Position</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
            </div>
          </div>
        </div>
        
    </div><!-- /.container-fluid -->
</div>
@endsection

@section('scripts')
<script>
    $(function () {
        $("#employeetable").DataTable({
            "responsive": true, "lengthChange": false, "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print"]
        }).buttons().container().appendTo('#employeetable_wrapper .col-md-6:eq(0)');
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
        });
    });
</script>
@endsection

@section('plugincss')

<link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/jqvmap/jqvmap.min.css') }}">
<link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.min.css') }}">

<link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">

@endsection



@section('javascript')

<script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('plugins/sparklines/sparkline.js') }}"></script>
<script src="{{ asset('plugins/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
<script src="{{ asset('plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<script src="{{ asset('dist/js/adminlte.js') }}"></script>

<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>

@endsection