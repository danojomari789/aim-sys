<!-- 
    | =================================================================================
    |                      Add Employee
    | =================================================================================
-->
<div class="modal fade" id="addemployee">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">
            <i class="nav-icon fas fa-user-plus"></i>&nbsp;Add Employee
        </h4>
          <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-times"></i></span>
          </button>
        </div>
        <div class="modal-body">
          <div class="container-fluid">

           {{-- <div class="row">
                <div class="col-md-12 form-group">
                    <ul id="save_errlist"></ul>
                </div>
           </div> --}}
            <div class="row">
                <div class="col-md-4 form-group">
                    <label for="firstnameinputAdd">First Name<span style="color:red">*</span></label>
                        <input 
                            class="form-control"
                            type="text"
                            id="firstnameinputAdd"
                            name="firstnameinputAdd"
                        >
                    <div id="err_fname">

                    </div>
                </div>
                <div class="col-md-4 form-group">
                    <label for="lastnameinputAdd">Last Name<span style="color:red">*</span></label>
                        <input 
                            class="form-control"
                            type="text"
                            id="lastnameinputAdd"
                            name="lastnameinputAdd"
                        >
                        <div id="err_lname">

                        </div>
                </div>
                <div class="col-md-4 form-group">
                    <label for="middlenameinputAdd">Middle Name</label>
                        <input 
                            class="form-control"
                            type="text"
                            id="middlenameinputAdd"
                            name="middlenameinputAdd"
                        >
                        <div id="err_mname">

                        </div>
                </div> 
            </div>
            <div class="row">
                <div class="col-md-6 form-group">
                    <label for="birthdayinputAdd">Birthday<span style="color:red">*</span></label>
                        <input 
                            class="form-control"
                            type="date"
                            id="birthdayinputAdd"
                            name="birthdayinputAdd"
                        >
                        <div id="err_bday">

                        </div>
                </div>
                <div class="col-md-6 form-group">
                    <label for="sexinputAdd">Sex<span style="color:red">*</span></label>
                        <select 
                            class="form-control" 
                            style="width: 100%;"
                            id="sexinputAdd"
                            name="sexinputAdd"
                            required
                        >
                            <option selected="selected" disabled>Sex*</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                        <div id="err_sex">

                        </div>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 form-group">
                    <label for="addressinputAdd">Address<span style="color:red">*</span></label>
                        <input 
                            class="form-control"
                            type="text"
                            id="addressinputAdd"
                            name="addressinputAdd"
                        >
                        <div id="err_address">

                        </div>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 form-group">
                    <label for="contnuminputAdd">Contact Number<span style="color:red">*</span></label>
                        <input 
                            class="form-control"
                            type="number"
                            id="contnuminputAdd"
                            name="contnuminputAdd"
                        >
                        <div id="err_contnum">

                        </div>
                        
                </div>
            </div>
            {{-- <div class="row">
                <div class="col-md-6 form-group">
                    <label for="emailinputAdd">Email<span style="color:red">*</span></label>
                        <input 
                            class="form-control"
                            type="text"
                            id="emailinputAdd"
                            name="emailinputAdd"
                        >
                </div>
                <div class="col-md-6 form-group">
                    <label for="passwordinputAdd">Password<span style="color:red">*</span></label>
                        <input 
                            class="form-control"
                            type="password"
                            id="firstnameinputAdd"
                            name="firstnameinputAdd"
                        >
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 form-group">
                    <label for="firstnameinputAdd">Role<span style="color:red">*</span></label>
                        <input 
                            class="form-control"
                            type="text"
                            id="firstnameinputAdd"
                            name="firstnameinputAdd"
                        >
                </div>
            </div> --}}
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary add_employee">Add Employee</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- 
    | =================================================================================
    |                      View Employee Details
    | =================================================================================
    -->
<div class="modal fade" id="viewemployee">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <i class="fas fa-file-alt"></i>&nbsp;&nbsp;
          <h4 class="modal-title">View Employee Record</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <!-- Patient Name -->
            <div class="container-fluid">

              <div class="row">
                <div class="col-sm-4">
                  <div class="card card-primary card-outline">
                    <h6 class="card-header">Employee Information</h6>
                    <div class="card-body">
      
                        <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle"
                                src="../../dist/img/user4-128x128.jpg"
                                alt="User profile picture">
                        </div>

                        <h3 class="profile-username text-center" id="profilenameView"></h3>
                        <br>

                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                            <b>Email</b> <a class="float-right">admin@admin.com</a>
                            </li>
                            <li class="list-group-item">
                            <b>Contact</b> <a class="float-right" id="contactnuminputViews"></a>
                            </li>
                        </ul>

                        
      
                    </div>
                  </div>
                </div>

                <div class="col-sm-8">
                    <div class="card card-primary card-outline">
                      <h6 class="card-header">Employee Information</h6>
                      <div class="card-body">
        
                        <div class="row">
                          <div class="col-sm-12">
                            <div class="d-flex align-items-baseline">
                              <div>
                                <label for="firstnameinputView">Name:</label>
                                <a id="firstnameinputView"></a>
                              </div>
                            </div>
                          </div>
                          
                        </div>
        
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="d-flex align-items-baseline">
                                <div>
                                    <label>Sex:&nbsp;</label>
                                </div>
                                <div class="d-flex align-items-baseline">
                                    <div class="icon-container">
    
                                    </div>
                                    <a id="sexinputView">&nbsp;</a>
                                </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="d-flex align-items-baseline">
                                <div>
                                    <label>Age:&nbsp;</label>
                                </div>
                                <div>
                                    <p id="ageinputView"></p>
                                </div>
                                </div>
                            </div>
                          
                        </div>
        
                        {{-- <div class="row">
                          <div class="col-sm-6">
                            <div class="d-flex align-items-baseline">
                              <div>
                                <label>Weight:&nbsp;</label>
                              </div>
                              <div>
                                <p id="weightnameinputView">None</p>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="d-flex align-items-baseline">
                              <div>
                                <label>Height:&nbsp;</label>
                              </div>
                              <div>
                                <p id="heightinputView">None</p>
                              </div>
                            </div>
                          </div>
                        </div> --}}
        
                        <div class="row">
                            
                            <div class="col-sm-6">
                                <div class="d-flex align-items-baseline">
                                <div>
                                    <label>Contact Number:&nbsp;</label>
                                </div>
                                <div>
                                    <p id="contactnuminputView">None</p>
                                </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="d-flex align-items-baseline">
                                  <div>
                                    <label>Birthday:&nbsp;</label>
                                  </div>
                                  <div>
                                    <p id="bdayinputView">None</p>
                                  </div>
                                </div>
                            </div>
                        </div>
        
                        <div class="row">
                          <div class="col-sm-12">
                            <div class="d-flex align-items-baseline">
                              <div>
                                <label>Address:&nbsp;</label>
                              </div>
                              <div>
                                <p id="addressinputView">None</p>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                              <div class="d-flex align-items-baseline">
                                <div>
                                  <label>Status:&nbsp;</label>
                                </div>
                                <div>
                                  <p id="statusinputView">None</p>
                                </div>
                              </div>
                            </div>
                        </div>

                      </div>
                    </div>
                  </div>
              </div>

            </div> 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-muted" data-dismiss="modal">Close</button>
      </div>
        
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- 
    | =================================================================================
    |                      Update Employee Details
    | =================================================================================
    -->
<div class="modal fade" id="updateemployee">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">
            <i class="nav-icon fas fa-user-plus"></i>&nbsp;Update Employee
        </h4>
          <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-times"></i></span>
          </button>
        </div>
        <div class="modal-body">
            <form id="updateemployeeForm">
                <div class="container-fluid">

                    {{-- <div class="row">
                            <div class="col-md-12 form-group">
                                <ul id="save_errlist"></ul>
                            </div>
                    </div> --}}
                        <div class="row">
                        <div class="col-md-4 form-group">
                            <label for="firstnameeditAdd">First Name<span style="color:red">*</span></label>
                                <input 
                                    class="form-control"
                                    type="text"
                                    id="firstnameeditAdd"
                                    name="firstnameeditAdd"
                                >
                            <div id="editerr_fname">

                            </div>
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="lastnameeditAdd">Last Name<span style="color:red">*</span></label>
                                <input 
                                    class="form-control"
                                    type="text"
                                    id="lastnameeditAdd"
                                    name="lastnameeditAdd"
                                >
                                <div id="editerr_lname">

                                </div>
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="middlenameeditAdd">Middle Name</label>
                                <input 
                                    class="form-control"
                                    type="text"
                                    id="middlenameeditAdd"
                                    name="middlenameeditAdd"
                                >
                                <div id="editerr_mname">

                                </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="birthdayeditAdd">Birthday<span style="color:red">*</span></label>
                                <input 
                                    class="form-control"
                                    type="date"
                                    id="birthdayeditAdd"
                                    name="birthdayeditAdd"
                                >
                                <div id="editerr_bday">

                                </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="sexeditAdd">Sex<span style="color:red">*</span></label>
                                <select 
                                    class="form-control" 
                                    style="width: 100%;"
                                    id="sexeditAdd"
                                    name="sexeditAdd"
                                    required
                                >
                                    <option selected="selected" disabled>Sex*</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                                <div id="editerr_sex">

                                </div>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label for="addresseditAdd">Address<span style="color:red">*</span></label>
                                <input 
                                    class="form-control"
                                    type="text"
                                    id="addresseditAdd"
                                    name="addresseditAdd"
                                >
                                <div id="editerr_address">

                                </div>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label for="contnumeditAdd">Contact Number<span style="color:red">*</span></label>
                                <input 
                                    class="form-control"
                                    type="number"
                                    id="contnumeditAdd"
                                    name="contnumeditAdd"
                                >
                                <div id="editerr_contnum">

                                </div>
                                
                        </div>
                    </div>
                </div>
            </form>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary update_employee">Save changes</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<!-- 
    | =================================================================================
    |                      Inactive Employee Details
    | =================================================================================
    -->
<div class="modal fade" id="inactiveemployee" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <i class="fas fa-user-check"></i>
        <h5 class="modal-title" id="exampleModalLongTitle">&nbspUpdate Status Employee</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form id="addroomForm">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <label for="statusinputEdit">Status</label>
                        <select 
                            class="form-control select2" 
                            style="width: 100%;"
                            id="statusinputEdit"
                            name="statusinputEdit"
                            required
                        >
                            <option selected disabled>Select Status*</option>
                            <option value="Active">Active</option>
                            <option value="In-active">In-active</option>
                        </select>
                    </div>
                    <div id="editerr_status">
    
                    </div>
                </div>
            </div>
            
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary updatestatus_employee">Save Changes</button>
      </div>

      </form> 
    </div>
  </div>
</div>